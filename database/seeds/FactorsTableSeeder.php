<?php

use App\Factor;
use Illuminate\Database\Seeder;

class FactorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Factor::create([
            'name' => 'Тест создания фактора',
            'symbol' => 'TEST'
        ]);

        factory(App\Factor::class, 2)->create();
    }
}
