<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToUndertestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('undertests', function (Blueprint $table) {
            $table->integer('type_text')->nullable();
            $table->integer('type_graph')->nullable();
            $table->string('graph_x')->nullable();
            $table->string('graph_y')->nullable();
            $table->string('text_x')->nullable()->change();
            $table->string('text_y')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('undertests', function (Blueprint $table) {
            $table->dropColumn('type_text');
            $table->dropColumn('type_graph');
            $table->dropColumn('graph_x');
            $table->dropColumn('graph_y');
            $table->string('text_x')->change();
            $table->string('text_y')->change();

        });
    }
}
