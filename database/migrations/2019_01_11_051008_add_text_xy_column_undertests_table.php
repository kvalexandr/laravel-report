<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTextXyColumnUndertestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('undertests', function (Blueprint $table) {
            $table->string('text_x');
            $table->string('text_y');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('undertests', function(Blueprint $table){
            $table->dropColumn('text_x');
            $table->dropColumn('text_y');
        });

    }
}
