<?php

namespace App\Http\Controllers;

use App\Factor;
use App\Test;
use App\Undertest;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;

class UndertestsController extends Controller
{

    public function index()
    {
        $undertests = Undertest::all();
        return view('undertests.index', ['undertests' => $undertests]);
    }

    public function create()
    {
        $factorsAll = Factor::all();
        $testAll = Test::all();
        return view('undertests.add', compact('testAll', 'factorsAll'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $undertest = Undertest::create($request->all());
        $undertest->setFactors($request->get('factors'));
        $undertest->setTest($request->get('test'));
        return redirect()->route('undertests.index');
    }

    public function show(Undertest $undertest)
    {
        return redirect()->route('undertests.index');
    }

    public function edit(Undertest $undertest)
    {
        $factorsUndertest = $undertest->factors()->orderBy('factor_undertest.id')->pluck('factor_id')->all();
        $factorsAll = Factor::all();
        $testAll = Test::all();
        return view('undertests.edit', compact('undertest', "factorsUndertest", "factorsAll", "testAll"));
    }

    public function update(Request $request, Undertest $undertest)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $undertest->update($request->all());
        $undertest->setFactors($request->get('factors'));
        $undertest->setTest($request->get('test'));
        return redirect()->route('undertests.index');
    }

    public function destroy(Undertest $undertest)
    {
        $undertest->delete();
        return redirect()->route('undertests.index');
    }

}
