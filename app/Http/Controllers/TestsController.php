<?php

namespace App\Http\Controllers;

use App\Test;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TestsController extends Controller
{

    public function index()
    {
        $tests = Test::all();
        return view('tests.index', compact('tests'));
    }

    public function create()
    {
        return view('tests.add');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $test = Test::create($request->all());
        $test->addFile($request->file('file'));

        return redirect()->route('tests.index');
    }

    public function edit(Test $test)
    {
        return view('tests.edit', ["test" => $test]);
    }

    public function update(Request $request, Test $test)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $test->update($request->all());
        $test->addFile($request->file('file'), $test->file);

        return redirect()->route('tests.index');
    }

    public function destroy(Test $test)
    {
        Storage::delete($test->file);
        $test->delete();
        return redirect()->route('tests.index');
    }


}
