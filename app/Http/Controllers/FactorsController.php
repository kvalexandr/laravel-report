<?php

namespace App\Http\Controllers;

use App\Factor;
use Illuminate\Http\Request;

class FactorsController extends Controller
{

    public function index()
    {
        $factors = Factor::paginate(10);
        return view('factors.index', ['factors' => $factors]);
    }

    public function create()
    {
        return view('factors.add');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'symbol' => 'required'
        ]);

        Factor::create($request->all());
        return redirect()->route('factors.index');
    }

    public function show(Factor $factor)
    {
        return redirect()->route('factors.index');
    }

    public function edit(Factor $factor)
    {
        return view('factors.edit', ['factor' => $factor]);
    }

    public function update(Request $request, Factor $factor)
    {
        $this->validate($request, [
            'name' => 'required',
            'symbol' => 'required'
        ]);

        $factor->update($request->all());
        return redirect()->route('factors.index');
    }

    public function destroy(Factor $factor)
    {
        $factor->delete();
        return redirect()->route('factors.index');
    }

}
