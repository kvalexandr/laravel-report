<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Factor extends Model
{
    public $guarded = [];

    public function undertests()
    {
        return $this->belongsToMany(Undertest::class);
    }
}
