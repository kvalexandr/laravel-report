<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Undertest extends Model
{
    public $guarded = ['factors', 'test'];

    public function factors()
    {
        return $this->belongsToMany(Factor::class);
    }

    public function test()
    {
        return $this->belongsTo(Test::class);
    }

    public function setFactors($factors_id)
    {
        $factors_id = array_diff($factors_id, array(''));
        $this->factors()->detach();
        if ($factors_id) {
            $this->factors()->attach($factors_id);
        }
    }

    public function setTest($test_id)
    {
        if ($test_id) {
            $this->test_id = $test_id;
            $this->save();
        }
    }
}
