<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Test extends Model
{
    protected $guarded = ['file'];


    public function undertests()
    {
        return $this->hasMany(Undertest::class);
    }

    public function addFile($file, $file_update = "")
    {
        if (!$file) return;
        if($file_update) Storage::delete($file_update);

        $filename = "test_" . $this->id . ".csv";
        $this->file = $file->storeAs('tests', $filename);
        $this->save();
    }

}
