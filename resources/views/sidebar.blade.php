<ul class="sidebar-menu">
    <li class="header">NAVIGATION</li>
    <li class="treeview">
        <a href="{{route('home')}}">
            <i class="fa fa-dashboard"></i> <span>Админ-панель</span>
        </a>
    </li>

    <li class="header">ТЕСТЫ</li>

    <li><a href="{{route('tests.index')}}"><i class="fa fa-bookmark"></i> <span>Тесты</span></a></li>
    <li><a href="{{route('undertests.index')}}"><i class="fa fa-bookmark"></i> <span>Подтесты</span></a></li>
    <li><a href="{{route('factors.index')}}"><i class="fa fa-flag"></i> <span>Факторы</span></a></li>

    <li class="header">СЛОВЕСНЫЕ ИНТЕРПРЕТАЦИИ</li>

    <li><a href=""><i class="fa fa-book"></i> <span>Категории</span></a></li>
    <li><a href=""><i class="fa fa-book"></i> <span>Предложения</span></a></li>

</ul>