@extends('layout')

@section('content')

    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Добавить фактор</h3>
                @include('errors')
            </div>

            <form action="{{route('factors.store')}}" method="post">

                @include('factors._form')
            </form>

        </div>
    </section>

@endsection