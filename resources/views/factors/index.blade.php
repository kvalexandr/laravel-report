@extends('layout')

@section('content')

    <section class="content-header">
        <h1>Факторы</h1>
    </section>


    <section class="content">

        <div class="box">
            <div class="box-body">
                <div class="form-group">
                    <a href="{{route('factors.create')}}" class="btn btn-success">Добавить</a>
                </div>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th>Символ</th>
                        <th width="200px"></th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($factors as $factor)
                        <tr>
                            <td><a href="{{route('factors.edit', $factor)}}">{{$factor->name}}</a></td>
                            <td>{{$factor->symbol}}</td>
                            <td>
                                <form onsubmit="if(confirm('Вы действительно хотите удалить?')){ return true }else{ return false }"
                                      action="{{route('factors.destroy', $factor)}}" method="post">
                                    <input type="hidden" name="_method" value="DELETE">
                                    {{ csrf_field() }}
                                    <div class="btn-group">
                                        <button type="submit" class="btn btn-danger">Удалить</button>
                                        <a href="{{route('factors.edit', $factor)}}" class="btn btn-warning">Редактировать</a>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>

                {{$factors->links()}}
            </div>
        </div>
    </section>

@endsection