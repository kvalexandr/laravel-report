@extends('layout')

@section('content')

    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Редактировать фактор #{{$factor->id}}</h3>
                @include('errors')
            </div>

            <form action="{{route('factors.update', $factor)}}" method="post">
                <input type="hidden" name="_method" value="put">

                @include('factors._form')
            </form>

        </div>
    </section>

@endsection