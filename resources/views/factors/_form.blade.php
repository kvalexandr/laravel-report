{{csrf_field()}}
<div class="box-body">
    <div class="col-md-6">
        <div class="form-group">
            <label for="name">Название</label>
            <input type="text" class="form-control" id="name" value="{{$factor->name ?? old('name')}}" name="name">
        </div>
        <div class="form-group">
            <label for="symbol">Символ</label>
            <input type="text" class="form-control" id="symbol" value="{{$factor->symbol ?? old('symbol')}}"
                   name="symbol">
        </div>
        <input type="submit" value="Сохранить" class="btn btn-success">
    </div>
</div>