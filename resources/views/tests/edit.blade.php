@extends('layout')

@section('content')

    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Редактировать тест #{{$test->id}}</h3>
                @include('errors')
            </div>

            <form action="{{route('tests.update', $test)}}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="put">

                @include('tests._form')
            </form>

        </div>
    </section>

@endsection