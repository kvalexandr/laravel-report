@extends('layout')

@section('content')

    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Добавить тест</h3>
                @include('errors')
            </div>

            <form action="{{route('tests.store')}}" method="post" enctype="multipart/form-data">

                @include('tests._form')
            </form>

        </div>
    </section>

@endsection