@extends('layout')

@section('content')

    <section class="content-header">
        <h1>Тесты</h1>
    </section>


    <section class="content">

        <div class="box">
            <div class="box-body">
                <div class="form-group">
                    <a href="{{route('tests.create')}}" class="btn btn-success">Добавить</a>
                </div>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Название</th>
                        <th>Описание</th>
                        <th>Файл</th>
                        <th width="200px"></th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($tests as $test)
                        <tr>
                            <td>{{$test->id}}</td>
                            <td><a href="{{route('tests.edit', $test)}}">{{$test->name}}</a></td>
                            <td>{{$test->description}}</td>
                            <td>{{$test->file}}</td>
                            <td>
                                <form onsubmit="if(confirm('Вы действительно хотите удалить?')){ return true }else{ return false }"
                                      action="{{route('tests.destroy', $test)}}" method="post">
                                    <input type="hidden" name="_method" value="DELETE">
                                    {{ csrf_field() }}
                                    <div class="btn-group">
                                        <button type="submit" class="btn btn-danger">Удалить</button>
                                        <a href="{{route('tests.edit', $test)}}" class="btn btn-warning">Редактировать</a>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </section>

@endsection