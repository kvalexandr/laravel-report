{{csrf_field()}}
<div class="box-body">
    <div class="col-md-6">
        <div class="form-group">
            <label for="name">Название</label>
            <input type="text" class="form-control" value="{{$test->name ?? old('name')}}" name="name">
        </div>
        <div class="form-group">
            <label for="name">Описание</label>
            <textarea class="form-control" name="description">
                {{$test->description ?? old('description')}}
            </textarea>
        </div>
        <div class="form-group">
            <label for="name">Файл</label>
            <input type="file" class="form-control" value="" name="file">
            @if($test->file ?? '')
                Файл с тестами: {{$test->file}}
            @endif
        </div>

        @if(isset($test->undertests))
            <div class="form-group">
                <label for="name">Подтесты</label>
                <br>
                @foreach($test->undertests as $undertest)
                    {{$undertest->name}}<br>
                @endforeach
            </div>
        @endif

        <input type="submit" value="Сохранить" class="btn btn-success">
    </div>
</div>