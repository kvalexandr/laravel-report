<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>АСИ | Report</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" href='/admin/bootstrap/css/bootstrap.min.css'>
    <link rel="stylesheet" href='/admin/font-awesome/4.5.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href='/admin/ionicons/2.0.1/css/ionicons.min.css'>
    <link rel="stylesheet" href='/admin/plugins/iCheck/minimal/_all.css'>
    <link rel="stylesheet" href='/admin/plugins/datepicker/datepicker3.css'>
    <link rel="stylesheet" href='/admin/plugins/select2/select2.min.css'>
    <link rel="stylesheet" href='/admin/plugins/datatables/dataTables.bootstrap.css'>
    <link rel="stylesheet" href='/admin/dist/css/AdminLTE.min.css'>
    <link rel="stylesheet" href='/admin/dist/css/skins/_all-skins.min.css'>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>А</b>СИ</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Report </b>АСИ</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="hidden-xs">Alexander</span>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            @include('sidebar')
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- =============================================== -->

    <div class="content-wrapper">
        @yield('content')
    </div>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">

        </div>
        <strong>АСИ Report &copy; {{date("Y")}}.</strong>
    </footer>


    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<script src='/admin/plugins/jQuery/jquery-2.2.3.min.js'></script>
<script src='/admin/bootstrap/js/bootstrap.min.js'></script>
<script src='/admin/plugins/select2/select2.full.min.js'></script>
<script src='/admin/plugins/datepicker/bootstrap-datepicker.js'></script>
<script src='/admin/plugins/datatables/jquery.dataTables.min.js'></script>
<script src='/admin/plugins/datatables/dataTables.bootstrap.min.js'></script>
<script src='/admin/plugins/slimScroll/jquery.slimscroll.min.js'></script>
<script src='/admin/plugins/fastclick/fastclick.js'></script>
<script src='/admin/plugins/iCheck/icheck.min.js'></script>
<script src='/admin/dist/js/app.min.js'></script>
<script src='/admin/dist/js/demo.js'></script>
<script src='/admin/dist/js/scripts.js'></script>
<script src="/admin/plugins/ckeditor/ckeditor.js"></script>
<script src="/admin/plugins/ckfinder/ckfinder.js"></script>
<script>
    $(document).ready(function () {
        var editor = CKEDITOR.replaceAll();
        CKFinder.setupCKEditor(editor);

        $(document).on("click", ".btn-add-elem", function (e) {
            var select = $(this).parent().find(".block-elements").html();
            $(".block-elements-add").append(select);
        });
    })

</script>

<style>
    .dataTables_info, .dataTables_paginate, .dataTables_length, .dataTables_filter {
        display: none;
    }
</style>
</body>
</html>
