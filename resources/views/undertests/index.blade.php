@extends('layout')

@section('content')

    <section class="content-header">
        <h1>Подтесты</h1>
    </section>


    <section class="content">

        <div class="box">
            <div class="box-body">
                <div class="form-group">
                    <a href="{{route('undertests.create')}}" class="btn btn-success">Добавить</a>
                </div>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Название</th>
                        <th>Символы</th>
                        <th width="200px"></th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($undertests as $undertest)
                        <tr>
                            <td>{{$undertest->id}}</td>
                            <td><a href="{{route('undertests.edit', $undertest)}}">{{$undertest->name}}</a></td>
                            <td>{{implode(", ",$undertest->factors()->orderBy('factor_undertest.id')->pluck('symbol')->all())}}</td>
                            <td>
                                <form onsubmit="if(confirm('Вы действительно хотите удалить?')){ return true }else{ return false }"
                                      action="{{route('undertests.destroy', $undertest)}}" method="post">
                                    <input type="hidden" name="_method" value="DELETE">
                                    {{ csrf_field() }}
                                    <div class="btn-group">
                                        <button type="submit" class="btn btn-danger">Удалить</button>
                                        <a href="{{route('undertests.edit', $undertest)}}" class="btn btn-warning">Редактировать</a>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </section>

@endsection