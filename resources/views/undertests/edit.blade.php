@extends('layout')

@section('content')

    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Редактировать подтест #{{$undertest->id}}</h3>
                @include('errors')
            </div>

            <form action="{{route('undertests.update', $undertest)}}" method="post">
                <input type="hidden" name="_method" value="put">

                @include('undertests._form')
            </form>

        </div>
    </section>

@endsection