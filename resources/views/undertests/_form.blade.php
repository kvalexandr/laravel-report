{{csrf_field()}}
<div class="box-body">
    <div class="col-md-6">
        <div class="form-group">
            <label for="name">Название</label>
            <input type="text" class="form-control" id="name" value="{{$undertest->name ?? old('name')}}" name="name">
        </div>
        <div class="form-group">
            <label for="name">Прикрепить к тесту</label>
            <select name="test" class="form-control">
                <option value="0">Выберите тест</option>
                @if (isset($testAll))
                    @if(isset($undertest->test))
                        @foreach($testAll as $test)
                            <option @if($undertest->test->id == $test->id){{'selected'}}@endif value="{{$test->id}}">
                                {{$test->name}}
                            </option>
                        @endforeach
                    @else
                        @foreach($testAll as $test)
                            <option value="{{$test->id}}">{{$test->name}}</option>
                        @endforeach
                    @endif
                @endif
            </select>
        </div>
    </div>
</div>

<div class="box-body">
    <div class="col-md-6">
        <div class="form-group">
            <label>Как составлять текстовое описание</label>
            <select name="type_text" class="form-control">
                <option value="0">Выберите тип</option>
                @if (isset($undertest->type_graph))
                    <option @if($undertest->type_text == 1){{'selected'}}@endif value="1">
                        На основе значений факторов
                    </option>
                    <option @if($undertest->type_text == 2){{'selected'}}@endif value="2">
                        Выбирать максимальное из всех факторов
                    </option>
                @else
                    <option value="1">На основе значений факторов</option>
                    <option value="2">Выбирать максимальное из всех факторов</option>
                @endif
            </select>
        </div>

        <div class="form-group">
            <label>Вид графика</label>
            <select name="type_graph" class="form-control">
                <option value="0">Выберите тип графика</option>
                @if (isset($undertest->type_graph))
                    <option @if($undertest->type_graph == 1){{'selected'}}@endif value="1">
                        Линейная диаграмма
                    </option>
                    <option @if($undertest->type_graph == 2){{'selected'}}@endif value="2">
                        Круговая диаграмма
                    </option>
                    <option @if($undertest->type_graph == 3){{'selected'}}@endif value="3">
                        Гистограмма
                    </option>
                @else
                    <option value="1">Линейная диаграмма</option>
                    <option value="2">Круговая диаграмма</option>
                    <option value="3">Гистограмма</option>
                @endif
            </select>
        </div>
    </div>
</div>

<div class="box-body">
    <div class="col-md-12">
        <div class="form-group">
            <label for="name">Составлять тексты по колонкам</label>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <input type="text" class="form-control" value="{{$undertest->text_x ?? old('text_x')}}" name="text_x">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <input type="text" class="form-control" value="{{$undertest->text_y ?? old('text_y')}}" name="text_y">
        </div>
    </div>
</div>
<div class="box-body">
    <div class="col-md-12">
        <div class="form-group">
            <label for="name">Составлять график по колонкам</label>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <input type="text" class="form-control" value="{{$undertest->graph_x ?? old('graph_x')}}" name="graph_x">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <input type="text" class="form-control" value="{{$undertest->graph_y ?? old('graph_y')}}" name="graph_y">
        </div>
    </div>
</div>
<div class="box-body">
    <div class="col-md-6">
        <div class="form-group">
            <label for="factors">Факторы</label>

            @if(isset($factorsUndertest))
                @foreach($factorsUndertest as $factorSelect)
                    <select class="form-control" name="factors[]">
                        <option value="">Удалить</option>
                        @foreach($factorsAll as $factor)
                            <option value="{{$factor->id}}"
                                    <?if($factor->id == $factorSelect):?>selected<?endif?>>
                                {{$factor->name}} ({{$factor->symbol}})
                            </option>
                        @endforeach
                    </select>
                @endforeach
            @endif

            <div class="block-elements">
                <select class="form-control" name="factors[]">
                    <option value="">Выберите новый фактор</option>
                    @foreach($factorsAll as $factor)
                        <option value="{{$factor->id}}">
                            {{$factor->name}} ({{$factor->symbol}})
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="block-elements-add"></div>

            <button type="button" class="btn btn-block btn-default btn-add-elem">Добавить фактор
            </button>
        </div>
        <input type="submit" value="Сохранить" class="btn btn-success">
    </div>
</div>