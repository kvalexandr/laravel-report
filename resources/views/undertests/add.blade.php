@extends('layout')

@section('content')

    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Добавить подтест</h3>
                @include('errors')
            </div>

            <form action="{{route('undertests.store')}}" method="post">

                @include('undertests._form')
            </form>

        </div>
    </section>

@endsection