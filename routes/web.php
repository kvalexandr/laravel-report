<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

Route::group(['middleware' => ['auth']], function () {
    Route::resource('/factors', 'FactorsController');
    Route::resource('/undertests', 'UndertestsController');
    Route::resource('/tests', 'TestsController');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
